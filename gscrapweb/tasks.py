from __future__ import absolute_import
from celery import shared_task
from gscrapweb.models import Track
from bs4 import BeautifulSoup
import re
import json
import base64
import quopri
import logging
import requests
import urlparse

from .scraping import scrape_gmail

@shared_task
def scrape(google_social_auth_id, start, end):
	scrape_gmail(google_social_auth_id, start, end)

def remove_tags(text):
	TAG_RE = re.compile(r'<[^>]+>')
	return TAG_RE.sub(' ', text)
